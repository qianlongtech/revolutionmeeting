$(document).ready(function () {
    $('html').fadeIn();
    $('.cover').find('img').each(function (index, element) {
        $(this).delay(index * 1000).fadeIn();
    });
    $('.cover').click(function () {
        $(this).fadeOut();
        $('.contain').fadeIn();
        $('.contain').find('li').each(function (index, element) {
            $(this).delay(index * 500).fadeIn();
        });
    });
});
